package ca.qc.claurendeau.exception;

public class BufferEmptyException extends Exception {
    
    @Override
    public String toString() {
        String str = "Buffer is empty";
        return str;
    }}
