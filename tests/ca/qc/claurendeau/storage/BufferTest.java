package ca.qc.claurendeau.storage;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import ca.qc.claurendeau.exception.BufferEmptyException;
import ca.qc.claurendeau.exception.BufferFullException;

public class BufferTest {
    
  
    Buffer buff;
    Element elem0;
    Element elem1;
    Element elem2;
    Element elem3;
    Element elem4;
    Element elem5;
    
    @Before
    public void setUp() {
        buff = new Buffer(5);
        elem0 = new Element();
        elem1 = new Element();
        elem2 = new Element();
        elem3 = new Element();
        elem4 = new Element();
        elem5 = new Element();
        elem0.setData(1);
        elem1.setData(2);
        elem2.setData(3);
        elem3.setData(4);
    }
    
    @After
    public void tearDown() {
        buff = null;
        elem0 = null;
        elem1 = null;
        elem2 = null;
        elem3 = null;
        elem4 = null;
        elem5 = null;
    }
    
    @Test
    public void testBuildSuccess() {
        assertTrue(true);
    }
    
    @Test
    public void testCapacity() {
        assertEquals(5, buff.capacity() );
    }
    
    @Test
    public void testToString() throws BufferFullException {
        buff.addElement(elem0);
        buff.addElement(elem1);
        buff.addElement(elem2);
        buff.addElement(elem3);
        assertEquals("1, 2, 3, 4", buff.toString()); 
    }
    
    @Test
    public void testToStringEmpty() {
        assertEquals("buffer is empty", buff.toString()); 
    }
    
    @Test
    public void testGetCurrentLoad() {
        assertEquals(0, buff.getCurrentLoad());
    }
    
    @Test
    public void testIsEmpty() {
        assertTrue(buff.isEmpty());
    }
    
    @Test
    public void testIsEmptyPlein() throws BufferFullException{
       buff.addElement(elem0);
       assertFalse(buff.isEmpty());
    }
    
    @Test
    public void testIsFull() {
        assertFalse(buff.isFull());
    }
    
    @Test
    public void testAddElement() throws BufferFullException{
        buff.addElement(elem0);
        assertTrue(buff.getDebut() == elem0);
        assertTrue(buff.getFin() == elem0);
        assertTrue(buff.getCurrentLoad() == 1);   
    }
    
    @Test
    public void testAddElementNonVide() throws BufferFullException{
        buff.addElement(elem0);
        buff.addElement(elem1);
        assertTrue(buff.getDebut() == elem0);
        assertTrue(buff.getFin() == elem1);
        assertTrue(buff.getCurrentLoad() == 2);   
    }
    
    @Test (expected =  BufferFullException.class)
    public void testAddElementFull() throws BufferFullException{
        buff.addElement(elem0);
        buff.addElement(elem1);
        buff.addElement(elem2);
        buff.addElement(elem3);
        buff.addElement(elem4);
        buff.addElement(elem5);   
    }
    
    @Test (expected =  BufferEmptyException.class)
    public void testRemoveElementEmpty() throws BufferEmptyException{
        buff.removeElement();       
    }
    
    @Test
    public void testRemoveElementDernier() throws BufferEmptyException, BufferFullException{
        buff.addElement(elem0);
        Element el = buff.removeElement();
        assertTrue(elem0 == el);
        assertTrue(null == buff.getFin());
        assertTrue(null == buff.getDebut());
        assertTrue(0 == buff.getCurrentLoad());
    }
    
    @Test
    public void testRemoveElement() throws BufferEmptyException, BufferFullException{
        buff.addElement(elem0);
        buff.addElement(elem1);
        Element el = buff.removeElement();
        assertTrue(elem0 == el);
        assertTrue(elem1 == buff.getFin());
        assertTrue(elem1 == buff.getDebut());
        assertTrue(1 == buff.getCurrentLoad());
    }
}


