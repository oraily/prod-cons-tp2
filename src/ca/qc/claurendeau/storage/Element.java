package ca.qc.claurendeau.storage;

public class Element
{
    private int data;
    private Element next;

    public Element getNext() {
        return next;
    }

    public void setNext(Element next) {
        this.next = next;
    }

    public void setData(int data)
    {
        this.data = data;
    }
    
    public int getData()
    {
        return data;
    }
    
    
}
