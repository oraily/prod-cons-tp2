package ca.qc.claurendeau.exception;

public class BufferFullException extends Exception {
    
    @Override
    public String toString() {
        String str = "Buffer is full";
        return str;
    }
}
