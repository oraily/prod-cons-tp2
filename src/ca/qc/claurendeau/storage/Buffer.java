package ca.qc.claurendeau.storage;

import ca.qc.claurendeau.exception.BufferEmptyException;
import ca.qc.claurendeau.exception.BufferFullException;

public class Buffer
{
    private int capacity;
    private int currentLoad;
    private Element debut;
    private Element fin;

    public Buffer(int capacity)
    {
        this.capacity = capacity;
        this.currentLoad = 0;
        this.debut= null;
        this.fin = null;
    }

    // returns the content of the buffer in form of a string
    public String toString()
    {
        String str = "buffer is empty";
        Element courant = debut;
        if(null != courant) {
            str = String.valueOf(courant.getData());
            while(courant.getNext()!= null) {
                str = str + ", " + courant.getNext().getData();
                courant = courant.getNext();
            }
        }
        return str;
    }

    // returns the capacity of the buffer
    public int capacity()
    {
        return this.capacity;
    }

    // returns the number of elements currently in the buffer
    public int getCurrentLoad()
    {
        return this.currentLoad;
    }

    // returns true if buffer is empty, false otherwise
    public boolean isEmpty()
    {
        if(0 == currentLoad && null == debut && null == fin) {
            return true;
        }
        return false;
    }

    // returns true if buffer is full, false otherwise
    public boolean isFull()
    {
        if(currentLoad == capacity) {
            return true;
        }
        return false;
    }

    // pour une queue, ajout a la fin
    // adds an element to the buffer
    // Throws an exception if the buffer is full
    public synchronized void addElement(Element element)throws BufferFullException // throws BufferFullException
    {
        if(isFull()) {
            throw new BufferFullException();
        }
        else {
            if(null == debut && null == fin) {
                debut = element;
                fin = element;
                currentLoad++;
            }
            else {
                fin.setNext(element);
                setFin(element);
                currentLoad++;
            }
        }
    }

    // pour une queue, retrait au debut
    // removes an element and returns it
    // Throws an exception if the buffer is empty
    public synchronized Element removeElement() throws BufferEmptyException// throws BufferEmptyException
    {
        if(isEmpty()) {
            throw new BufferEmptyException();
        }
        else {
            if(1 == currentLoad && debut == fin) {
                Element el  = debut;
                fin = null;
                debut = null;
                currentLoad -- ;
                return el;
            }
            else {
                Element el  = debut;
                setDebut(debut.getNext());
                currentLoad --;
                return el;
            }
        }
    }

    public Element getDebut() {
        return debut;
    }

    public void setDebut(Element elem) {
        this.debut = elem;
    }

    public Element getFin() {
        return fin;
    }

    public void setFin(Element elem) {
        this.fin = elem;
    }
}
